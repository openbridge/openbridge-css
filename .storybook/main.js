

module.exports = {
  stories: ['../scss/**/*.stories.[tj]s','../scss/**/*.stories.[tj]sx'],
  addons: ['@storybook/addon-storysource/register',"@storybook/addon-links",
    "@storybook/addon-essentials"]
};
