export default {title: 'Dimming menu'};

export const normal = () => `
<div class="ob-dimming-menu">
    <div class="ob-section">
         <div class="ob-heading">Day - Night</div>
         <div class="ob-toggle-switch">
          <div class="ob-toggle-switch__label">Auto</div>
          <div class="ob-toggle-switch__button ob-selected">
              <div class="ob-toggle-switch__thumb"></div>
          </div>
          <div class="ob-toggle-switch__divider"></div>
        </div>
        <div class="ob-toggle-button-icon ob-py-2">
          <div class="ob-toggle-button-icon__container">
               <div class="ob-toggle-button-icon__item">
                  <div class="ob-icon"><span class="mdi mdi-moon-waning-crescent"></span></div>
                  <div class="ob-label">Night</div>
               </div>
               <div class="ob-toggle-button-icon__item">
                  <div class="ob-icon">
                    <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path fill-rule="evenodd" clip-rule="evenodd" d="M12 9.99997C9.39105 9.99997 7.16727 11.6712 6.34234 14H17.6577C16.8328 11.6712 14.609 9.99997 12 9.99997ZM4.96002 7.49997L6.76002 9.28997L5.34002 10.7L3.55002 8.90997L4.96002 7.49997ZM11 4.99997H13V7.94997H11V4.99997ZM19.04 7.49997L20.45 8.90997L18.66 10.7L17.25 9.28997L19.04 7.49997ZM1.00002 17V15H23V17H1.00002Z" fill="currentColor"/>
                    </svg>
                  </div>
                  <div class="ob-label">Dusk</div>
               </div>
               <div class="ob-toggle-button-icon__item ob-selected">
                  <div class="ob-icon">
                    <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path fill-rule="evenodd" clip-rule="evenodd" d="M4.96 4.04999L6.76 5.83999L5.34 7.24999L3.55 5.45999L4.96 4.04999ZM19.04 4.04999L20.45 5.45999L18.66 7.24999L17.25 5.83999L19.04 4.04999ZM6 12.5C6 9.18999 8.69 6.49998 12 6.49998C13.8948 6.49998 15.5864 7.38145 16.6865 8.75609C16.1875 8.62169 15.6623 8.54999 15.12 8.54999C12.8273 8.54999 10.836 9.8375 9.84433 11.7217C8.28342 11.8859 6.94962 12.7981 6.21297 14.0879C6.07415 13.582 6 13.0495 6 12.5ZM19.9776 14.1525C19.5222 11.7806 17.5224 10 15.12 10C13.2126 10 11.556 11.1275 10.731 12.7775C8.7444 12.9975 7.2 14.7506 7.2 16.875C7.2 19.1506 8.9754 21 11.16 21H19.74C21.5616 21 23.04 19.46 23.04 17.5625C23.04 15.7475 21.687 14.2762 19.9776 14.1525ZM4 11.5H1V13.5H4V11.5ZM13 1.54999H11V4.49999H13V1.54999Z" fill="currentColor"/>
                    </svg>
                  </div>
                  <div class="ob-label">Day</div>
               </div>
               <div class="ob-toggle-button-icon__item">
                  <div class="ob-icon">
                        <span class="mdi mdi-white-balance-sunny"></span>
                  </div>
                  <div class="ob-label">Bright</div>
               </div>
          </div>
        </div>
    </div>
    <div class="ob-section">
        <div class="ob-heading">Display Brilliance</div>
         <div class="ob-toggle-switch">
          <div class="ob-toggle-switch__label">Auto</div>
          <div class="ob-toggle-switch__button">
              <div class="ob-toggle-switch__thumb"></div>
          </div>
          <div class="ob-toggle-switch__divider"></div>
        </div>
        <div class="ob-slider ob-py-2">
          <div class="ob-slider__container ">
             <div class="ob-slider__edge_item ob-left">
                  <div class="ob-icon">
                    <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path fill-rule="evenodd" clip-rule="evenodd" d="M11 4H13V6H11V4ZM16 12C16 14.2091 14.2091 16 12 16C9.79086 16 8 14.2091 8 12C8 9.79086 9.79086 8 12 8C14.2091 8 16 9.79086 16 12ZM18.364 7.05025L16.9497 5.63604L15.5355 7.05025L16.9497 8.46447L18.364 7.05025ZM4 13V11H6L6 13H4ZM7.05025 5.63604L5.63604 7.05025L7.05025 8.46446L8.46447 7.05025L7.05025 5.63604ZM13 18V20H11V18H13ZM8.46447 16.9498L7.05025 15.5355L5.63604 16.9498L7.05025 18.364L8.46447 16.9498ZM18 11H20V13H18V11ZM16.9498 15.5355L15.5355 16.9497L16.9498 18.364L18.364 16.9497L16.9498 15.5355Z" fill="currentColor"/>
                    </svg>
                  </div>
             </div>
             <div class="ob-slider__edge_item ob-right">
                  <div class="ob-icon">
                        <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path fill-rule="evenodd" clip-rule="evenodd" d="M11 2H13V6H11V2ZM16 12C16 14.2091 14.2091 16 12 16C9.79086 16 8 14.2091 8 12C8 9.79086 9.79086 8 12 8C14.2091 8 16 9.79086 16 12ZM19.7782 5.63604L18.364 4.22182L15.5355 7.05025L16.9497 8.46447L19.7782 5.63604ZM2 13V11H6V13H2ZM5.63605 4.22183L4.22183 5.63604L7.05026 8.46447L8.46447 7.05025L5.63605 4.22183ZM13 18V22H11V18H13ZM8.46446 16.9498L7.05025 15.5355L4.22182 18.364L5.63603 19.7782L8.46446 16.9498ZM18 11H22V13H18V11ZM16.9498 15.5355L15.5355 16.9497L18.364 19.7782L19.7782 18.364L16.9498 15.5355Z" fill="currentColor"/>
                        </svg>
                  </div>
             </div>
             <div class="ob-slider__item" style="left: calc(.4 * (100% - 6rem))">
                40
             </div>
            </div>
        </div>
    </div>
    <div class="ob-section ob-list-item">
        <div class="ob-icon">
            <span class="mdi mdi-monitor"></span>
        </div>
        <div class="ob-label">Display settings</div>
        <div class="ob-icon">
            <span class="mdi mdi-open-in-new"></span>
        </div>
    </div>
</div>`;
