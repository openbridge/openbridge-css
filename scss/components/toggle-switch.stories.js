export default {title: 'Toggle switch'};

export const normal = () => `<div class="ob-toggle-switch__button"><div class="ob-toggle-switch__thumb"></div></div>`;
export const selected = () => `<div class="ob-toggle-switch__button ob-selected"><div class="ob-toggle-switch__thumb"></div></div>`;
export const normalDisable = () => `<div class="ob-toggle-switch__button ob-disabled"><div class="ob-toggle-switch__thumb"></div></div>`;
export const selectedDisable = () => `<div class="ob-toggle-switch__button ob-selected ob-disabled"><div class="ob-toggle-switch__thumb"></div></div>`;

export const normalLabel = () => `
<div class="ob-toggle-switch" style="width: 192px">
  <div class="ob-toggle-switch__label">Label</div>
  <div class="ob-toggle-switch__button">
      <div class="ob-toggle-switch__thumb"></div>
  </div>
  <div class="ob-toggle-switch__divider"></div>
</div>`;
