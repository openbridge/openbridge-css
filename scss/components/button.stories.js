export default {title: 'Button'};

export const normal = () => `
<a class="ob-button ob-button--normal">
    <span class="ob-label">Button</span>
</a>`;
export const normalDisabled = () => `<a class="ob-button ob-button--normal" disabled><span class="ob-label">Button</span></a>`;
export const normalWithIcon = () => `<a class="ob-button ob-button--normal">
<span class="ob-icon"><i class="material-icons">add</i></span><span class="ob-label">Button</span></a>`;
export const raised = () => '<a class="ob-button ob-button--raised"><span class="ob-label">Button</span></a>';

export const iconNormal = () => '<a class="ob-button ob-button--icon ob-button--supressed"><span class="ob-icon mdi mdi-account"></span></a>';

export const iconAlarm = () => '<a class="ob-button ob-button--icon ob-button--supressed ob-button--alarm"><span class="ob-icon mdi mdi-bell"></span><span class="ob-button__counter">12</span></a>';
export const iconWarning = () => '<a class="ob-button ob-button--icon ob-button--supressed ob-button--warning"><span class="ob-icon mdi mdi-bell"></span></a>'
export const iconCaution = () => '<a class="ob-button ob-button--icon ob-button--supressed ob-button--caution"><span class="ob-icon mdi mdi-bell"></span></a>'
