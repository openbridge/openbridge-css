export default {title: 'Navigation menu'};

export const normal = () => `
<div style="width: 100%; height: 100%; position: absolute">
<div class="ob-nav-menu">
  <div class="ob-nav-menu-body">
    <div class="ob-nav-list-item">
      <div class="ob-icon ob-nav-list-item__icon">
        <span class="mdi mdi-ferry"></span>
      </div>
      <div class="ob-label">List item</div>
      <div class="ob-badge ob-badge--alarm">
          <span class="ob-text">18</span>
      </div>
      <div class="ob-badge ob-badge--in-command">
        <span class="ob-icon mdi mdi-account-circle"></span>
      </div>
    </div>
    <div class="ob-nav-list-item  ob-active">
      <div class="ob-icon ob-nav-list-item__icon">
        <span class="mdi mdi-ferry"></span>
      </div>
      <div class="ob-label">List item</div>
    </div>
  </div>

  <div class="ob-nav-menu-footer">
    <div class="ob-nav-menu-footer-item">
        <div class="ob-nav-menu-footer-item__icon ob-icon">
            <span class="mdi mdi-cog"></span>
        </div>
        <div class="ob-nav-menu-footer-item__label">
            Settings
        </div>
        <div class="ob-nav-menu-footer-item__divider"></div>
    </div>
    <div class="ob-nav-menu-footer-item">
        <div class="ob-nav-menu-footer-item__icon ob-icon">
            <span class="mdi mdi-help-circle"></span>
        </div>
        <div class="ob-nav-menu-footer-item__label">
            Help & Support
        </div>
        <div class="ob-nav-menu-footer-item__divider"></div>
    </div>
    <div class="ob-nav-menu-footer-item">
        <div class="ob-nav-menu-footer-item__icon ob-icon">
            <span class="mdi mdi-power-standby"></span>
        </div>
        <div class="ob-nav-menu-footer-item__label">
            Standby
        </div>
        <div class="ob-nav-menu-footer-item__divider"></div>
    </div>
    <div class="ob-nav-menu-footer-branding">
        <div class="ob-nav-menu-footer-branding__icon">
            <span class="mdi mdi-account-circle"></span>
        </div>
        <div class="ob-nav-menu-footer-branding__name">
            Maritime
        </div>
    </div>
  </div>
</div>
</div>
`;
