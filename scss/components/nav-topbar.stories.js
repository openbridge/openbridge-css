export default {title: 'Topbar'};

export const normal = () => `
<div class="ob-nav-top-bar">
  <div class="ob-menu-container">
    <div class="ob-button ob-button--supressed ob-button--icon">
        <span class="ob-icon mdi mdi-menu"></span>
    </div>
    <div class="ob-divider"> </div>
    <div class="ob-title">App</div>
    <div class="ob-sub-title">Section</div>
  </div>
  <div class="ob-menu-container">
    <div class="ob-clock">14:34</div>
    <div class="ob-divider"> </div>
    <div class="ob-nav-top-bar__apps">
      <a class="ob-app-btn ob-small">
          <div class="ob-btn-icon ob-icon mdi mdi-ferry"></div>
      </a>
      <a class="ob-app-btn ob-small">
          <div class="ob-btn-icon ob-icon mdi mdi-ferry"></div>
      </a>
      <a class="ob-app-btn ob-small">
          <div class="ob-btn-icon ob-icon mdi mdi-ferry"></div>
      </a>
    </div>
    <div class="ob-divider"> </div>
    <div class="ob-button ob-button--supressed ob-button--icon">
        <span class="ob-icon mdi mdi-bell"></span>
    </div>
    <div class="ob-button ob-button--supressed ob-button--icon">
        <span class="ob-icon mdi mdi-account-circle"></span>
    </div>
    <div class="ob-button ob-button--supressed ob-button--icon">
        <span class="ob-icon mdi mdi-brightness-4 mdi-rotate-180"></span>
    </div>
    <div class="ob-button ob-button--supressed ob-button--icon">
        <span class="ob-icon mdi mdi-apps"></span>
    </div>
  </div>
</div>
`;
