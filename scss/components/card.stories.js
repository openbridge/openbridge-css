export default {title: 'Card'};

export const regular = () => `
<div style="background: var(--container-backdrop-color); position: absolute; top:0; bottom:0; left: 0; right: 0; padding: 20px">
  <div class="ob-card">
      <div class="ob-card-header">
          <div class="ob-card-header-title-icon"><span class="ob-icon mdi mdi-account-circle"></span> </div>
          <div class="ob-card-header-title">Title</div>
          <div class="ob-card-header-action-icon">
            <a class="ob-button ob-button--icon ob-button--supressed"><span class="ob-icon mdi mdi-pencil"></span></a>
          </div>
      </div>
      <div class="ob-card-body">
          <div style="height: 70vh; width: 50vw;"></div>
      </div>
  </div>
</div>`;

