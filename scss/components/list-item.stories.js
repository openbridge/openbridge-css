export default {title: 'List item'};

export const iconLabel = () => `
<div style="width: 256px">
<div class="ob-nav-list-item">
<div class="ob-icon ob-nav-list-item__icon">
    <span class="mdi mdi-ferry"></span>
</div>
<div class="ob-label">List item</div>
<div class="ob-badge ob-badge--alarm">
<span class="ob-text">18</span>
</div>
<div class="ob-badge ob-badge--in-command">
    <span class="ob-icon mdi mdi-account-circle"></span>
</div>
</div>
</div>`;

export const iconLabelActive = () => `
<div style="width: 256px">
<div class="ob-nav-list-item ob-active">
<div class="ob-icon ob-nav-list-item__icon">
    <span class="mdi mdi-ferry"></span>
</div>
<div class="ob-label">List item</div>
</div>
</div>`;

export const label = () => `
<div style="width: 256px">
<div class="ob-nav-list-item">
<div class="ob-label">List item</div>
</div>
</div>`;

export const iconLabelCollapsed = () => `
<div style="width: 256px">
<div class="ob-nav-list-item">
<div class="ob-icon ob-nav-list-item__chevron">
    <span class="mdi mdi-chevron-right"></span>
</div>
<div class="ob-icon ob-nav-list-item__icon">
    <span class="mdi mdi-ferry"></span>
</div>
<div class="ob-label">List item</div>
<div class="ob-badge ob-badge--alarm">
<span class="ob-text">18</span>
</div>
<div class="ob-badge ob-badge--in-command">
    <span class="ob-icon mdi mdi-account-circle"></span>
</div>
</div>
</div>`;

export const iconLabelExpanded = () => `
<div style="width: 256px">
<div class="ob-nav-list-item">
<div class="ob-icon">
    <span class="mdi mdi-chevron-down"></span>
</div>
<div class="ob-icon ob-nav-list-item__icon">
    <span class="mdi mdi-ferry"></span>
</div>
<div class="ob-label">List item</div>
<div class="ob-badge ob-badge--alarm">
<span class="ob-text">18</span>
</div>
<div class="ob-badge ob-badge--in-command">
    <span class="ob-icon mdi mdi-account-circle"></span>
</div>
</div>
</div>`;

export const iconLabelLevel2 = () => `
<div style="width: 256px">
<div class="ob-nav-list-item ob-level-2">
<div class="ob-icon">
    <span class="mdi mdi-chevron-right"></span>
</div>
<div class="ob-icon ob-nav-list-item__icon">
    <span class="mdi mdi-ferry"></span>
</div>
<div class="ob-label">List item</div>
</div>`;


export const iconLabelLevel3 = () => `
<div style="width: 256px">
<div class="ob-nav-list-item ob-level-3">
<div class="ob-icon">
    <span class="mdi mdi-chevron-right"></span>
</div>
<div class="ob-icon ob-nav-list-item__icon">
    <span class="mdi mdi-ferry"></span>
</div>
<div class="ob-label">List item</div>
</div>`;

export const iconLabelFlyOut = () => `
<div style="width: 256px">
<div class="ob-nav-list-item">
<div class="ob-icon">
    <span class="mdi mdi-chevron-right"></span>
</div>
<div class="ob-icon ob-nav-list-item__icon">
    <span class="mdi mdi-ferry"></span>
</div>
<div class="ob-label">
    List item
    <span class="ob-icon ob-nav-list-item__flyout mdi mdi-menu-right"></span>
</div>
</div>`;

export const icon = () => `
<div class="ob-nav-list-item ob-nav-list-item--icon">
<div class="ob-icon ob-nav-list-item__icon">
    <span class="mdi mdi-ferry"></span>
</div>
<div class="ob-badge ob-badge--alarm">
    <span class="ob-text">9</span>
</div>
<div class="ob-badge ob-badge--in-command">
    <span class="ob-icon mdi mdi-account-circle"></span>
</div>
<span class="ob-icon ob-nav-list-item__flyout mdi mdi-menu-right"></span>
</div>`;
