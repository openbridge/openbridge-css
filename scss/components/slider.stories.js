export default {title: 'Slider'};

export const normal = () => `

<div class="ob-slider">
    <div class="ob-slider__container">
        <div class="ob-slider__track"></div>
        <div class="ob-slider__track--selected" style="right: 50%"></div>
         <div class="ob-slider__item" style="left: calc(.5 * (100% - 6rem))">
            50
         </div>
    </div>
</div>`;
